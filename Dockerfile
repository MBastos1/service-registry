FROM openjdk:11-jdk-oraclelinux8
LABEL maintainer="pucminas.com"

VOLUME /tmp
EXPOSE 8761

ARG JAR_FILE=target/service-registry*.jar
ADD ${JAR_FILE} service-registry.jar

ENTRYPOINT ["sh", "-c", "java -jar /service-registry.jar"]

